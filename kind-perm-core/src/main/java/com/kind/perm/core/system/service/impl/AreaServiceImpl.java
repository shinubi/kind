package com.kind.perm.core.system.service.impl;

import java.util.List;

import com.kind.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.system.dao.AreaDao;
import com.kind.perm.core.system.domain.AreaDO;
import com.kind.perm.core.system.service.AreaService;

/**
 * 
 * 区域信息业务处理实现类. <br/>
 *
 * @date:2017-03-01 09:53:29 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class AreaServiceImpl implements AreaService {

	@Autowired
	private AreaDao areaDao;

	@Override
	public PageView<AreaDO> selectPageList(PageQuery pageQuery) {
        pageQuery.setPageSize(pageQuery.getPageSize());
		List<AreaDO> list = areaDao.page(pageQuery);
		int count = areaDao.count(pageQuery);
        pageQuery.setItems(count);
		return new PageView<>(pageQuery, list);
	}

	@Override
	public int save(AreaDO entity) throws ServiceException {
		try {
           return areaDao.saveOrUpdate(entity);
        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }

	}

	@Override
	public AreaDO getById(Long id) {
		return areaDao.getById(id);
	}

    @Override
    public void remove(Long id) throws ServiceException{
        try {
            areaDao.remove(id);

        }catch (Exception e){
            throw new ServiceException(e.getMessage());
        }
    }

	@Override
	public List<AreaDO> queryList(AreaDO entity) {
		return areaDao.queryList(entity);
	}
	
	@Override
	public List<AreaDO> queryByPcode(String pcode) {
		return areaDao.queryByPcode(pcode);
	}

	@Override
	public AreaDO queryByCode(String code) {
		return areaDao.queryByCode(code);
	}
}