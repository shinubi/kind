package com.kind.perm.core.system.service;

import java.util.List;

import com.kind.perm.core.system.domain.CoFileDO;
import com.kind.perm.core.system.domain.FileDO;

/**
 * 文件处理接口<br/>
 *
 * @Date: 2016年12月12日
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public interface FileService {

    /**
     * 保存数据
     * @param entity
     */
	int save(FileDO entity);
	
    /**
     * 删除数据
     * @param id
     */
	void remove(Long id);
	
    /**
     * 获取数据对象
     * @param id
     * @return
     */
	FileDO getById(Long id);
	
    /**
     * 获取数据对象
     * @param entity
     * @return
     */
	List<FileDO> getFileDOlistByCoFileDO(CoFileDO entity);
	


}
