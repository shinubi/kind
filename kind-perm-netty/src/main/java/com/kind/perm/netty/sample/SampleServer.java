package com.kind.perm.netty.sample;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.kind.perm.netty.AppConfig;
import com.kind.perm.netty.BootServer;
import com.kind.perm.netty.Server;
import com.kind.perm.netty.utils.DefConfigFactory;

/**
 * 基于spring注解的sampleServer
 * User: 李明
 * Date: 2016/1/28
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
@Configuration
@ComponentScan
public class SampleServer {

    public static void main(String[] args) {
        // 启动spring容器
        ApplicationContext context = new AnnotationConfigApplicationContext(SampleServer.class);
        // 生成开发环境的配置
        AppConfig devConfig = DefConfigFactory.createDEVConfig();
        Server server = new BootServer(devConfig, context);
        server.start();
    }
}
