package com.kind.common.client.response;

import java.io.Serializable;
import java.util.List;

/**
 * Json响应结果对象【列表类型】
 * 
 * @author 李明
 *
 */
public class JsonResponseResultList<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 响应业务状态码[0-成功 非0-失败] */
    private Integer ret;
    /** 响应消息 */
    private String msg;
    /** 响应中的列表数据 */
    private List<T> data;
    
    public JsonResponseResultList(Integer ret, String msg, List<T> data) {
        this.ret = ret;
        this.msg = msg;
        this.data = data;
    }

    public JsonResponseResultList(Integer ret, String msg) {
        this.ret = ret;
        this.msg = msg;
        this.data = null;
    }
    
    public JsonResponseResultList(List<T> data) {
        this.ret = 0;
        this.msg = "OK";
        this.data = data;
    }
    
    public JsonResponseResultList() {

    }

    public Integer getRet() {
        return ret;
    }

    public void setRet(Integer ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

	public Object getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

}
